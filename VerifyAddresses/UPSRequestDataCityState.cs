﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VerifyAddresses
{
    class UPSRequestData
    {
        public UPSSecurityGroup AccessRequest;
        public Transaction AddressValidationRequest;
    }

    class UPSSecurityGroup
    {
        public string AccessLicenseNumber = "1D519566448E5175";
        public string UserId = "mmoore1984";
        public string Password = "w3stch3st3r!";
    }
    class Transaction
    {
        public RequestSettings Request;
        public AddressKey Address;
    }
    class RequestSettings
    {
        public string RequestAction = "AV";
        public TransReference TransactionReference;
    }
    class TransReference
    {
        public string CustomerContext;
    }
    class AddressKey
    {
        public string City;  //City
        public string StateProvinceCode;  //State
        public string PostCode;  //Zip
        public string CountryCode;
    }


}
