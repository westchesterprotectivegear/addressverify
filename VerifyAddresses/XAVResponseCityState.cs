﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VerifyAddresses
{
    public class UPSResponse
    {
        public AVResponse AddressValidationResponse;
    }

    public class AVResponse
    {
        public ResponseCodes Response;
        public List<AVResult> AddressValidationResult;
    }
    public class ResponseCodes
    {
        public TransactionContext TransactionReference;
        public string ResponseStatusCode;
        public string ResponseStatusDescription;
        public Errors Error;
    }
    public class Errors
    {
        public string ErrorSeverity;
        public string ErrorCode;
        public string ErrorDescription;
    }
    public class TransactionContext
    {
        public string CustomerContext;
    }
    public class AVResult
    {
        public string Rank;
        public string Quality;
        public string PostalCodeLowEnd;
        public string PostalCodeHighEnd;
        public AddressText Address;
    }
    public class AddressText
    {
        public string City;
        public string StateProvinceCode;
    }
    
}
