﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VerifyAddresses
{
    public class UPSResponse
    {
        public AVResponse XAVResponse;
        public Faults Fault;
    }

    public class Faults
    {
        public string faultcode;
        public string faultstring;
        public FaultDetails detail;
    }
    public class FaultDetails
    {
        public FaultError Errors;
    }
    public class FaultError
    {
        public FaultErrorDetails ErrorDetail;
    }
    public class FaultErrorDetails
    {
        public string Severity;
        public ErrorCodes PrimaryErrorCode;
    }
    public class ErrorCodes
    {
        public string Code;
        public string Description;
    }

    public class AVResponse
    {
        public ResponseCodes Response;
        public string ValidAddressIndicator;
        public string NoCandidatesIndicator;
        public string AmbiguousAddressIndicator;
        public List<PossibleAddresses> Candidate;
    }
    public class ResponseCodes
    {
        public TransactionContext TransactionReference;
        public ResponseStatusCodes ResponseStatus;
        public Errors Error;
    }
    public class ResponseStatusCodes
    {
        public string Code;
        public string Description;
    }
    public class Errors
    {
        public string ErrorSeverity;
        public string ErrorCode;
        public string ErrorDescription;
    }
    public class TransactionContext
    {
        public string CustomerContext;
    }
    public class PossibleAddresses
    {
        public AddressItem AddressKeyFormat;
    }
    public class AddressItem
    {
        public string AddressLine;
        public string PoliticalDivision2;   //City
        public string PoliticalDivision1;  //State
        public string PostcodePrimaryLow;  //Zip
        public string PostcodeExtendedLow;   //Extended Zip
        public string Region;                //Full City/State Zip
        public string CountryCode;
    }
}
