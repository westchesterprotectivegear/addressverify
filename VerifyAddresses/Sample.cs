namespace VerifyAddresses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Sample
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(10)]
        public string SampleNumber { get; set; }

        [StringLength(12)]
        public string ShiptoNumber { get; set; }

        [StringLength(50)]
        public string ShiptoName { get; set; }

        [StringLength(50)]
        public string ShiptoAddress { get; set; }

        [StringLength(50)]
        public string shiptoCity { get; set; }

        [StringLength(50)]
        public string ShiptoState { get; set; }

        [StringLength(10)]
        public string ShiptoZip { get; set; }

        [StringLength(50)]
        public string CustomerPO { get; set; }

        [StringLength(10)]
        public string StoreNumber { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(4)]
        public string ShippingType { get; set; }

        
        public DateTime DateDownloaded { get; set; }

        [StringLength(10)]
        public string LocationID { get; set; }

        [StringLength(20)]
        public string Route { get; set; }

        public decimal? ActualWeight { get; set; }

        [StringLength(40)]
        public string ProNumber { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Cartons { get; set; }

        [StringLength(10)]
        public string Inco1 { get; set; }

        [StringLength(50)]
        public string ShiptoName2 { get; set; }
        [StringLength(10)]
        public string BeforeUpdateZip { get; set; }
        [NotMapped]
        public bool Updated { get; set; }
    }
}
