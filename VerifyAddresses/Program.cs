﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Script.Serialization;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace VerifyAddresses
{
    class Program
    {
        static string q;
        public const string URL = "https://onlinetools.ups.com/rest/XAV";
        public static int totalmulticorrections;
        static void Main(string[] args)
        {
            string UPS_DOWNLOADS_FOLDER;
            string SAMPLES_FOLDER;
            totalmulticorrections = 0;
            try
            {
                UPS_DOWNLOADS_FOLDER = args[0];
                SAMPLES_FOLDER = args[1];
            } catch (Exception e)
            {
                UPS_DOWNLOADS_FOLDER = @"\\Filesrv\sap\ups\download\process\done\";
                SAMPLES_FOLDER = @"\\Filesrv\sap\ups\samples\process\done\";
            }
            string deliverynumber;
            string samplenumber;
            string VBELN;
            string d1;

            bool addressverified = false; ;
            Delivery del;
            Sample sam;
            SqlConnection conn;
            SqlCommand cmd;
            Verify context = new Verify();

            DirectoryInfo d = new DirectoryInfo(UPS_DOWNLOADS_FOLDER);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.txt"); //Getting Text files
            foreach (var file in Files)
            {
                StreamReader sr = file.OpenText();
                //Console.Out.WriteLine(file.Name);
                VBELN = deliverynumber = sr.ReadLine();
                if (deliverynumber.Length > 1 && deliverynumber[0] == '0')
                {
                    deliverynumber = Int32.Parse(deliverynumber).ToString();
                }
                d1 = DateTime.Now.ToString("mm/dd/yy hh:mm:tt");


                var item = context.Deliveries.Where(x => x.DeliveryNumber == deliverynumber);
                /*q = "SELECT * FROM Deliveries WHERE DeliveryNumber = @DN";
                conn = new SqlConnection(connectionstring);
                cmd = new SqlCommand(q, conn);
                cmd.Parameters.Add("@DN", System.Data.SqlDbType.VarChar);
                cmd.Parameters["@DN"].Value = deliverynumber;
                var results = cmd.ExecuteReader();
                */
                if (item.Count() == 0)
                {
                    del = new Delivery();
                }
                else
                {
                    del = item.Single();
                }
                del.Updated = false;
                del.DeliveryNumber = deliverynumber;
                del.ShiptoNumber = sr.ReadLine();
                del.ShiptoName = sr.ReadLine();
                //Console.WriteLine(deliverynumber + "  " + del.ShiptoNumber);
                if (!sr.EndOfStream)
                {
                    del.ShiptoAddress = sr.ReadLine();
                    if (del.ShiptoAddress.Length > 50) { del.ShiptoAddress = del.ShiptoAddress.Substring(1, 50); }
                }
                else { del.ShiptoAddress = ""; }
                if (!sr.EndOfStream)
                {
                    del.shiptoCity = sr.ReadLine();
                }
                else { del.shiptoCity = "??"; }
                if (!sr.EndOfStream)
                {
                    del.ShiptoState = sr.ReadLine();
                }
                else { del.ShiptoState = "XX"; }
                if (!sr.EndOfStream)
                {
                    del.ShiptoZip = sr.ReadLine();
                }
                else { del.ShiptoZip = "00000"; }
                if (!sr.EndOfStream)
                {
                    del.CustomerPO = sr.ReadLine();
                }
                else { del.CustomerPO = ""; }
                if (del.CustomerPO.Length > 30) { del.CustomerPO = del.CustomerPO.Substring(1, 30); }
                del.CustomerPO.Trim();
                if (!sr.EndOfStream)
                {
                    del.StoreNumber = sr.ReadLine();
                    //if the store number is missing replace it with the Ship To Number
                    if (del.ShiptoNumber.IndexOf('.') > -1 && del.StoreNumber.Length == 0)
                    {
                        del.StoreNumber = del.ShiptoNumber.Substring(del.ShiptoNumber.IndexOf('.') + 1);
                    }
                }
                else { del.StoreNumber = ""; }
                if (!sr.EndOfStream)
                {
                    string[] temp = sr.ReadLine().Split(' ');
                    del.Weight = decimal.Parse(temp[0].Replace(",", String.Empty));
                    del.DateDownloaded = DateTime.Now;
                }
                else
                {
                    del.Weight = 0;
                    del.DateDownloaded = DateTime.Now;
                }
                if (!sr.EndOfStream)
                {
                    del.ShippingType = sr.ReadLine();
                    if (del.ShiptoNumber.ToUpper().IndexOf("HO38") != -1)
                    {
                        del.LocationID = "HD9100";
                    }
                    else { del.LocationID = ""; }
                }
                else { del.ShippingType = ""; }
                if (!sr.EndOfStream)
                {
                    del.Route = sr.ReadLine();
                }
                else { del.Route = ""; }
                if (!sr.EndOfStream)
                {
                    string[] temp = sr.ReadLine().Split(' ');
                    del.ActualWeight = decimal.Parse(temp[0].Replace(",", String.Empty));
                }
                else
                { del.ActualWeight = 0; }

                if (!sr.EndOfStream)
                {
                    del.ProNumber = sr.ReadLine();
                }
                else { del.ProNumber = ""; }
                if (!sr.EndOfStream)
                {
                    del.Cartons = decimal.Parse(sr.ReadLine().Split(' ')[0].Replace(",", String.Empty));
                }
                else { del.Cartons = 0; }

                if (!sr.EndOfStream)
                {
                    del.Inco1 = sr.ReadLine().Trim();
                }
                else { del.Inco1 = ""; }
                if (!sr.EndOfStream)
                {
                    del.ShiptoName2 = sr.ReadLine().Trim();
                }
                else { del.ShiptoName2 = ""; }

                if (!sr.EndOfStream)
                {
                    string temp = sr.ReadLine();
                    temp = temp.Replace("-", "");
                    temp = temp.Replace("(", "");
                    temp = temp.Replace(")", "");
                    if (temp.Length > 10) { temp = temp.Substring(temp.Length - 9); }
                    if (Double.TryParse(temp, out double res)) { temp = Double.Parse(temp).ToString("0000000000"); }
                    del.PhoneNumber = temp;

                }
                else { del.PhoneNumber = ""; }
                q = "SELECT prd.LIKP.KUNAG, prd.LIKP.INCO2, prd.ADRC.COUNTRY, prd.ADR6.SMTP_ADDR ";
                q += "FROM prd.LIKP INNER JOIN ";
                q += " prd.VBPA ON prd.LIKP.MANDT = prd.VBPA.MANDT AND prd.LIKP.VBELN = prd.VBPA.VBELN INNER JOIN";
                q += " prd.ADRC ON prd.VBPA.MANDT = prd.ADRC.CLIENT AND prd.VBPA.ADRNR = prd.ADRC.ADDRNUMBER INNER JOIN";
                q += " prd.TVBUR ON prd.LIKP.MANDT = prd.TVBUR.MANDT AND prd.LIKP.VKBUR = prd.TVBUR.VKBUR LEFT OUTER JOIN";
                q += " prd.ADR6 ON prd.TVBUR.MANDT = prd.ADR6.CLIENT AND prd.TVBUR.ADRNR = prd.ADR6.ADDRNUMBER";
                q += " WHERE(prd.LIKP.MANDT = '100') AND(prd.LIKP.VBELN = '00" + deliverynumber + "') AND(prd.VBPA.PARVW = 'WE') ";


                /*
                                q = "SELECT       prd.LIKP.KUNAG, prd.LIKP.INCO2, prd.ADRC.COUNTRY ";
                                q += "FROM         prd.LIKP INNER JOIN ";
                                q += "             prd.VBPA ON prd.LIKP.MANDT = prd.VBPA.MANDT AND prd.LIKP.VBELN = prd.VBPA.VBELN INNER JOIN ";
                                q += "             prd.ADRC ON prd.VBPA.MANDT = prd.ADRC.CLIENT AND prd.VBPA.ADRNR = prd.ADRC.ADDRNUMBER ";
                                q += "WHERE       (prd.LIKP.MANDT = '100') AND (prd.LIKP.VBELN = '00" + deliverynumber + "') AND (prd.VBPA.PARVW = 'WE')";
                                */

                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPConnection"].ConnectionString);
                conn.Open();
                cmd = new SqlCommand(q, conn);
                var results = cmd.ExecuteReader();
                if (results.HasRows)
                {
                    results.Read();
                    del.SoldtoNumber = results["KUNAG"].ToString();
                    del.ShiptoCountry = results["COUNTRY"].ToString();
                    del.Inco2 = results["INCO2"].ToString();
                    del.SalesRep = results["SMTP_ADDR"].ToString();
                }
                else
                {
                    del.SoldtoNumber = "????";
                    del.ShiptoCountry = "????";
                    del.Inco2 = "";
                }
                if (del.SalesRep != null && del.SalesRep != "")
                {
                    del.SalesRep = GetCustomerRepresentative(del.SalesRep);
                }


                sr.Close();
                addressverified = ValidateUPSAddress(del);
                if (item.Count() == 0)
                {
                    context.Deliveries.Add(del);
                }
                try
                {
                    context.SaveChanges();
                    updateDeliveryOLDdb(del);
                } catch (Exception e)
                {
                    ProcessingExplosion(deliverynumber, file.FullName);
                }
                if (addressverified)
                {
                    if (File.Exists(UPS_DOWNLOADS_FOLDER + "testdone\\" + file.Name))
                    {
                        File.Delete(UPS_DOWNLOADS_FOLDER + "testdone\\" + file.Name);
                    }
                    file.MoveTo(UPS_DOWNLOADS_FOLDER + "testdone\\" + file.Name);
                }
                conn.Close();
                //Console.ReadLine();
            }
            //Console.WriteLine("Completed Deliveries");
            //Console.ReadLine();
            //***********************************************************************************************
            //********************************SAMPLES FILE MANAGEMENT ***************************************
            //***********************************************************************************************

            //Console.WriteLine(totalmulticorrections.ToString());
            //Console.ReadLine();
            d = new DirectoryInfo(SAMPLES_FOLDER);//Assuming Test is your Folder
            Files = d.GetFiles("*.txt"); //Getting Text files
            foreach (var file in Files)
            {
                StreamReader sr = file.OpenText();
                //Console.Out.WriteLine(file.Name);
                samplenumber = sr.ReadLine();
                samplenumber.TrimStart(new char[] { '0' });
                d1 = DateTime.Now.ToString("mm/dd/yy hh:mm:tt");

                var samp = context.Samples.Where(x => x.SampleNumber == samplenumber);
                //if there is no sample with that number, create a blank one, if there is, load the first one.
                if (samp.Count() < 1)
                {
                    sam = new Sample();
                } else { sam = samp.Single(); }
                sam.Updated = false;
                sam.SampleNumber = samplenumber;
                sam.ShiptoNumber = sr.ReadLine();
                sam.ShiptoName = sr.ReadLine();

                if (!sr.EndOfStream)
                {
                    sam.ShiptoAddress = sr.ReadLine();
                    if (sam.ShiptoAddress.Length > 50) { sam.ShiptoAddress = sam.ShiptoAddress.Substring(1, 50); }
                }
                else { sam.ShiptoAddress = ""; }
                if (!sr.EndOfStream)
                {
                    sam.shiptoCity = sr.ReadLine();
                }
                else { sam.shiptoCity = "??"; }
                if (!sr.EndOfStream)
                {
                    sam.ShiptoState = sr.ReadLine();
                }
                else { sam.ShiptoState = "XX"; }
                if (!sr.EndOfStream)
                {
                    sam.ShiptoZip = sr.ReadLine();
                }
                else { sam.ShiptoZip = "00000"; }
                if (!sr.EndOfStream)
                {
                    sam.CustomerPO = sr.ReadLine();
                }
                else { sam.CustomerPO = ""; }
                if (sam.CustomerPO.Length > 30) { sam.CustomerPO = sam.CustomerPO.Substring(1, 30); }
                sam.CustomerPO.Trim();
                if (!sr.EndOfStream)
                {
                    sam.StoreNumber = sr.ReadLine();
                    //if the store number is missing replace it with the Ship To Number
                    if (sam.ShiptoNumber.IndexOf('.') > -1 && sam.StoreNumber.Length == 0)
                    {
                        sam.StoreNumber = sam.ShiptoNumber.Substring(sam.ShiptoNumber.IndexOf('.') + 1);
                    }
                }
                else { sam.StoreNumber = ""; }
                if (!sr.EndOfStream)
                {
                    string[] temp = sr.ReadLine().Split(' ');
                    sam.Weight = decimal.Parse(temp[0].Replace(",", String.Empty));
                    sam.DateDownloaded = DateTime.Now;
                }
                else
                {
                    sam.Weight = 0;
                    sam.DateDownloaded = DateTime.Now;
                }
                sam.LocationID = "";
                sam.ShippingType = "";
                sam.Route = "";

                if (!sr.EndOfStream)
                {
                    var temp = sr.ReadLine().Split(' ');
                    if (temp.Length > 0)
                    {
                        temp[0] = temp[0].Replace(",", String.Empty);
                        if (!decimal.TryParse(temp[0], out decimal result))
                        {
                            sam.ActualWeight = 0;
                        }
                        else
                        {
                            sam.ActualWeight = result;
                        }
                    }
                    else { sam.ActualWeight = 0; }

                } else { sam.ActualWeight = 0; }
                sam.Inco1 = "";
                if (!sr.EndOfStream)
                {
                    sam.ShiptoName2 = sr.ReadLine().Trim();
                } else { sam.ShiptoName2 = ""; }

                sr.Close();


                addressverified = ValidateUPSAddress(sam);
                if (samp.Count() < 1)
                {
                    context.Samples.Add(sam);
                }
                try
                {
                    context.SaveChanges();
                    updateDeliveryOLDdb(sam);
                }
                catch (Exception e)
                {
                    ProcessingExplosion(samplenumber, file.FullName);
                }
            if (addressverified)
                {
                    if (File.Exists(SAMPLES_FOLDER + "testdone\\" + file.Name))
                    {
                        File.Delete(SAMPLES_FOLDER + "testdone\\" + file.Name);
                    }
                    file.MoveTo(SAMPLES_FOLDER + "testdone\\" + file.Name);
                }
            }
            //Console.WriteLine("Completed Samples");
            //Console.ReadLine();
        }

        public static bool ValidateUPSAddress(Delivery AddressInfo)
        {
            bool found = false;
            if (AddressInfo.ShiptoCountry != "CA")
            {
                UPSRequestData data = new UPSRequestData();
                data.UPSSecurity = new UPSSecurityGroup();
                data.XAVRequest = new Transaction();
                data.XAVRequest.Request = new RequestSettings();
                data.XAVRequest.AddressKeyFormat = new AddressKey();
                data.XAVRequest.Request.TransactionReference = new TransReference();
                data.UPSSecurity.ServiceAccessToken = new ServiceToken();
                data.UPSSecurity.UsernameToken = new UserToken();
                data.XAVRequest.Request.TransactionReference.CustomerContext = AddressInfo.CustomerPO;
                data.XAVRequest.AddressKeyFormat.ConsigneeName = AddressInfo.ShiptoName;
                data.XAVRequest.AddressKeyFormat.BuildingName = AddressInfo.ShiptoName2;
                data.XAVRequest.AddressKeyFormat.AddressLine = AddressInfo.ShiptoAddress;
                data.XAVRequest.AddressKeyFormat.PoliticalDivision2 = AddressInfo.shiptoCity;
                data.XAVRequest.AddressKeyFormat.PoliticalDivision1 = AddressInfo.ShiptoState;
                data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow = AddressInfo.ShiptoZip;
                data.XAVRequest.AddressKeyFormat.CountryCode = AddressInfo.ShiptoCountry;
                if (AddressInfo.ShiptoName2 == null)
                {
                    data.XAVRequest.AddressKeyFormat.BuildingName = "";
                }
                if (AddressInfo.CustomerPO == "")
                {
                    data.XAVRequest.Request.TransactionReference.CustomerContext = "CustomerOrder";
                }
                if (AddressInfo.ShiptoCountry == "????")
                {
                    data.XAVRequest.AddressKeyFormat.CountryCode = "US";
                }

                UPSResponse resp = ValidateCall(data);
                if (resp != null && resp.Fault == null && resp.XAVResponse.Candidate != null && resp.XAVResponse.NoCandidatesIndicator == null && resp.XAVResponse.Response.Error == null)
                {
                    foreach (var cand in resp.XAVResponse.Candidate)
                    {
                        if (AddressInfo.shiptoCity.ToUpper() == cand.AddressKeyFormat.PoliticalDivision2.ToUpper() &&
                            AddressInfo.ShiptoState.ToUpper() == cand.AddressKeyFormat.PoliticalDivision1.ToUpper() &&
                            AddressInfo.ShiptoZip.ToUpper() == cand.AddressKeyFormat.PostcodePrimaryLow.ToUpper() + "-" + resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodeExtendedLow.ToUpper())
                        {
                            found = true;
                        }
                    }
                    if (found == false && resp.XAVResponse.Candidate.Count() == 1 &&
                        AddressInfo.shiptoCity.ToUpper() == resp.XAVResponse.Candidate[0].AddressKeyFormat.PoliticalDivision2.ToUpper() &&
                    AddressInfo.ShiptoState.ToUpper() == resp.XAVResponse.Candidate[0].AddressKeyFormat.PoliticalDivision1.ToUpper() &&
                    AddressInfo.ShiptoZip.ToUpper() != resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() &&
                    AddressInfo.ShiptoZip.ToUpper().Trim() != resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() + "-" + resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodeExtendedLow.ToUpper())
                    {
                        string oldzip = AddressInfo.ShiptoZip;
                        AddressInfo.ShiptoZip = resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() + "-" + resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodeExtendedLow.ToUpper();
                        AddressInfo.BeforeUpdateZip = oldzip;
                        Console.WriteLine("Order Updated:  " + AddressInfo.CustomerPO + "From: " + oldzip + "   To:  " + AddressInfo.ShiptoZip);
                        AddressInfo.Updated = true;
                        if (AddressInfo.ShiptoNumber != "ONETIME" && !AddressInfo.ShiptoNumber.Contains(".1TIME"))
                        {
                            SendUpdatedEmail("Delivery", oldzip, AddressInfo.ShiptoZip, AddressInfo.ShiptoNumber, AddressInfo.SalesRep, AddressInfo.DeliveryNumber, AddressInfo.ShiptoAddress, AddressInfo.shiptoCity, AddressInfo.ShiptoState, AddressInfo.ShiptoName);
                        }
                    }
                    else if (!found && resp.XAVResponse.Candidate.Count > 1)
                    {
                        if (AddressInfo.ShiptoNumber != "ONETIME" && !AddressInfo.ShiptoNumber.Contains(".1TIME"))
                        {
                            totalmulticorrections++;
                            SendMultipleCorrectionEmail(resp, "Delivery", AddressInfo.ShiptoZip, AddressInfo.ShiptoNumber, AddressInfo.SalesRep, AddressInfo.DeliveryNumber, AddressInfo.ShiptoAddress, AddressInfo.shiptoCity, AddressInfo.ShiptoState, AddressInfo.ShiptoName);
                        }

                    }
                } else if (resp.Fault != null && AddressInfo.ShiptoNumber != "ONETIME" && !AddressInfo.ShiptoNumber.Contains(".1TIME"))
                {
                    SendLookUpErrorEmail(resp, "Delivery", AddressInfo.ShiptoZip, AddressInfo.ShiptoNumber, AddressInfo.SalesRep, AddressInfo.DeliveryNumber, AddressInfo.ShiptoAddress, AddressInfo.shiptoCity, AddressInfo.ShiptoState, AddressInfo.ShiptoName);
                }
                /*else if (resp != null && resp.XAVResponse.Candidate!=null && resp.XAVResponse.Candidate.AddressKeyFormat.Count > 1)
                {
                    // SendLookUpErrorEmail(resp, "Delivery", AddressInfo.CustomerPO, AddressInfo.ShiptoAddress, AddressInfo.shiptoCity, AddressInfo.ShiptoState, AddressInfo.ShiptoZip);
                }*/
            }

            return true;
        }
        public static bool ValidateUPSAddress(Sample AddressInfo)
        {
            bool found = false;
            UPSRequestData data = new UPSRequestData();
            data.UPSSecurity = new UPSSecurityGroup();
            data.XAVRequest = new Transaction();
            data.XAVRequest.Request = new RequestSettings();
            data.XAVRequest.AddressKeyFormat = new AddressKey();
            data.XAVRequest.Request.TransactionReference = new TransReference();
            data.UPSSecurity.ServiceAccessToken = new ServiceToken();
            data.UPSSecurity.UsernameToken = new UserToken();
            data.XAVRequest.Request.TransactionReference.CustomerContext = AddressInfo.CustomerPO;
            data.XAVRequest.AddressKeyFormat.ConsigneeName = AddressInfo.ShiptoName;
            data.XAVRequest.AddressKeyFormat.BuildingName = AddressInfo.ShiptoName2;
            data.XAVRequest.AddressKeyFormat.AddressLine = AddressInfo.ShiptoAddress;
            data.XAVRequest.AddressKeyFormat.PoliticalDivision2 = AddressInfo.shiptoCity;
            data.XAVRequest.AddressKeyFormat.PoliticalDivision1 = AddressInfo.ShiptoState;
            data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow = AddressInfo.ShiptoZip;
            data.XAVRequest.AddressKeyFormat.CountryCode = "US";
            if (AddressInfo.ShiptoName2 == null)
            {
                data.XAVRequest.AddressKeyFormat.BuildingName = "";
            }
            if (AddressInfo.CustomerPO == "")
            {
                data.XAVRequest.Request.TransactionReference.CustomerContext = "CustomerOrder";
            }

            UPSResponse resp = ValidateCall(data);
            if (resp != null && resp.Fault == null && resp.XAVResponse.Candidate != null && resp.XAVResponse.NoCandidatesIndicator == null && resp.XAVResponse.Response.Error == null)
            {
                foreach (var cand in resp.XAVResponse.Candidate)
                {
                    if (AddressInfo.shiptoCity.ToUpper() == cand.AddressKeyFormat.PoliticalDivision2.ToUpper() &&
                        AddressInfo.ShiptoState.ToUpper() == cand.AddressKeyFormat.PoliticalDivision1.ToUpper() &&
                        AddressInfo.ShiptoZip.ToUpper() == cand.AddressKeyFormat.PostcodePrimaryLow.ToUpper())
                    {
                        found = true;
                    }
                }
                if (AddressInfo.shiptoCity.ToUpper() == resp.XAVResponse.Candidate[0].AddressKeyFormat.PoliticalDivision2.ToUpper() &&
                AddressInfo.ShiptoState.ToUpper() == resp.XAVResponse.Candidate[0].AddressKeyFormat.PoliticalDivision1.ToUpper() &&
                (AddressInfo.ShiptoZip.ToUpper() != resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() &&
                 AddressInfo.ShiptoZip.ToUpper() != resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() + "-" + resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodeExtendedLow.ToUpper())
                 && found == false && resp.XAVResponse.Candidate.Count() == 1)
                {
                    string oldzip = AddressInfo.ShiptoZip;
                    AddressInfo.ShiptoZip = resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() + "-" + resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodeExtendedLow.ToUpper();
                    AddressInfo.BeforeUpdateZip = oldzip;
                    //Console.WriteLine("Order Updated:  " + AddressInfo.CustomerPO + "   From: " + oldzip + "   To:  " + AddressInfo.ShiptoZip);
                    AddressInfo.Updated = true;
                }
                else if (!found && resp.XAVResponse.Candidate.Count > 1)
                {
                    //SendMultipleCorrectionEmail(resp, "Delivery", AddressInfo.CustomerPO, AddressInfo.ShiptoAddress, AddressInfo.shiptoCity, AddressInfo.ShiptoState, AddressInfo.ShiptoZip);
                }
            }
            /*else if (resp != null && resp.XAVResponse.Candidate != null && resp.XAVResponse.Candidate.AddressKeyFormat.Count > 1)
            {
                // SendLookUpErrorEmail(resp, "Delivery", AddressInfo.CustomerPO, AddressInfo.ShiptoAddress, AddressInfo.shiptoCity, AddressInfo.ShiptoState, AddressInfo.ShiptoZip);
            }*/
            return true;
        }
        public static UPSResponse ValidateCall(UPSRequestData data)
        {
            string json = new JavaScriptSerializer().Serialize(data);
            var client = new HttpClient();
            //Console.WriteLine(json.ToString());
            HttpResponseMessage response = client.PostAsync(URL, new StringContent(json, Encoding.UTF8, "application/json")).Result;
            string responseText = PurifyTheUnclean(response.Content.ReadAsStringAsync().Result); //Cleans out arrays on the Address line and combines them to one single string.
            responseText = MakeCandidateArray(responseText);
            //Console.WriteLine(responseText);
            //Console.ReadLine();
            UPSResponse resp = new UPSResponse();


            try
            {
                resp = new JavaScriptSerializer().Deserialize<UPSResponse>(responseText);
            } catch (Exception e)
            {
                resp = null;
                Console.WriteLine("Bad Data File " + e.Message);
                Console.WriteLine(json.ToString());
                Console.WriteLine(responseText);
                //Console.ReadLine();
            }
            if (resp == null || resp.Fault != null)
            {
                //Console.WriteLine(json.ToString());
                //Console.WriteLine(responseText);
                //Console.ReadLine();
            }
            /* if (resp.AddressValidationResponse.Response.Error != null)
             {
                 Console.WriteLine(json.ToString());
                 Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                 Console.ReadLine();
             } */
            return resp;
        }

        public static string PurifyTheUnclean(string resp)
        {
            string retval = resp;
            int start = 13;
            int end = 0;
            int separators = 0;
            int prev = 0;
            while (start <= retval.Length && end >= 0 && start >= 13)
            {
                start = retval.IndexOf("AddressLine\":[", prev) + 13;
                end = retval.IndexOf("], \"PoliticalDivision2", start);
                /* Console.WriteLine(start.ToString());
                 Console.WriteLine(end.ToString());
                 Console.WriteLine(prev.ToString());
                 Console.WriteLine(resp);
                 Console.WriteLine(retval);
                 Console.ReadLine();*/

                if (start != 12 && end != -1)
                {
                    separators = retval.IndexOf("\", \"", start, end - start);
                    while (separators != -1)
                    {
                        try
                        {
                            retval = retval.Remove(separators, 4).Insert(separators, " ");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine();
                            Console.WriteLine();
                            Console.WriteLine(e.Message);
                            Console.WriteLine(resp);
                            Console.WriteLine(retval);
                            Console.ReadLine();
                        }
                        separators = retval.IndexOf("\", \"", start, end - start);
                    }

                }
                prev = start;
            }

            return retval.Replace("\"AddressLine\":[\"", "\"AddressLine\":\"").Replace("\"], \"PoliticalDivision2\":", "\", \"PoliticalDivision2\":");

        }
        public static string MakeCandidateArray(string resp)
        {
            bool foundcandidate = false;
            if (resp.IndexOf("\"Candidate\":{") != -1)
            {
                foundcandidate = true;
                resp = resp.Replace("\"Candidate\":{", "\"Candidate\":[{");
            }
            if (foundcandidate)
            {
                resp = resp.Replace("}}}}", "}}]}}");
            }
            return resp;
        }
        public static void SendUpdatedEmail(string type, string oldzip, string newzip, string CustomerNumber, string SalesRep, string OrderNumber, string ShiptoAddress, string shiptoCity, string ShiptoState, string ShiptoName)
        {
            SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
            string message = "";
            //string defaultRecipient = "mmoore@westchestergear.com";
            string defaultRecipient = SalesRep;
            if (defaultRecipient == null)
            {
                //message += q;
                defaultRecipient = "asm@westchestergear.com";
                Console.WriteLine(q);
            }
            MailMessage email = new MailMessage();
            email.To.Add(defaultRecipient);
            email.Subject = type + " Zip Code Discrepancy in " + type + " " + OrderNumber;
            email.From = new MailAddress("AddressUpdater@westchestergear.com");


            message += "Address for Customer " + CustomerNumber + " was updated:<br />";
            message += ShiptoName + "<br />";
            message += ShiptoAddress + "<br />";
            message += shiptoCity + ", " + ShiptoState + " " + oldzip + "<br /><br />";

            message += "The zip code was updated<br />";
            message += "From:  " + oldzip;
            message += "<br />To:  " + newzip;

            message += "<br /><br />The Zip code has been changed in the UPS Shipping database.<br />";
            message += "Please correct the Ship to Address in SAP.";

            email.Body = message;
            email.IsBodyHtml = true;
            // smtp.Send(email);
        }
        public static void SendMultipleCorrectionEmail(UPSResponse resp, string type, string oldzip, string CustomerNumber, string SalesRep, string OrderNumber, string ShiptoAddress, string shiptoCity, string ShiptoState, string ShiptoName)
        {
            string message = "";
            SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
            //string defaultRecipient = "mmoore@westchestergear.com";
            string defaultRecipient = SalesRep;
            if (defaultRecipient == null)
            {
                //message += q;
                defaultRecipient = "asm@westchestergear.com";
                Console.WriteLine(q);
            }
            MailMessage email = new MailMessage();
            email.To.Add(defaultRecipient);
            email.Subject = type + " Address Discrepancy in " + type + " " + OrderNumber;
            email.From = new MailAddress("AddressUpdater@westchestergear.com");


            message += "Address for Customer " + CustomerNumber + " may need updated from:<br />";
            message += ShiptoName + "<br />";
            message += ShiptoAddress + "<br />";
            message += shiptoCity + ", " + ShiptoState + " " + oldzip + "<br /><br />";

            message += "Possible changes are:<br /> ";
            foreach (var address in resp.XAVResponse.Candidate)
            {
                message += address.AddressKeyFormat.AddressLine + "<br />";
                message += address.AddressKeyFormat.PoliticalDivision2 + ", " + address.AddressKeyFormat.PoliticalDivision1 + " " + address.AddressKeyFormat.PostcodePrimaryLow + "<br /><br />";
            }

            message += "<br /><br />Please correct the Ship to Address in SAP.";

            email.Body = message;
            email.IsBodyHtml = true;
            smtp.Send(email);
        }

        public static void SendLookUpErrorEmail(UPSResponse resp, string type, string oldzip, string CustomerNumber, string SalesRep, string OrderNumber, string ShiptoAddress, string shiptoCity, string ShiptoState, string ShiptoName)
        {
            string message = "";
            SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
            //string defaultRecipient = "mmoore@westchestergear.com";
            string defaultRecipient = SalesRep;
            if (defaultRecipient == null)
            {
                //message += q;
                defaultRecipient = "asm@westchestergear.com";
                Console.WriteLine(q);
            }
            MailMessage email = new MailMessage();
            email.To.Add(defaultRecipient);
            email.Subject = type + " Address Issue in " + type + " " + OrderNumber;
            email.From = new MailAddress("AddressUpdater@westchestergear.com");

            message += "Address for Customer " + CustomerNumber + " may need updated:<br />";
            message += ShiptoName + "<br />";
            message += ShiptoAddress + "<br />";
            message += shiptoCity + ", " + ShiptoState + " " + oldzip + "<br /><br />";

            message += "The address could not be validated through UPS Validation.  Please verify that the address is correct within SAP.<br />";

            email.Body = message;
            email.IsBodyHtml = true;
            smtp.Send(email);
        }
        public static void updateDeliveryOLDdb(Delivery del)
        {
            string q = "";
            q = "Update Deliveries set ShiptoZip=@shiptozip where DeliveryNumber=@deliverynumber";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UPSFilesrv"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@shiptozip", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@shiptozip"].Value = del.ShiptoZip;
            cmd.Parameters.Add(new SqlParameter("@deliverynumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@deliverynumber"].Value = del.DeliveryNumber;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        public static void updateDeliveryOLDdb(Sample sam)
        {
            string q = "";
            q = "Update Samples set ShiptoZip=@shiptozip where SampleNumber=@samplenumber";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["UPSFilesrv"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(q, conn);
            cmd.Parameters.Add(new SqlParameter("@shiptozip", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@shiptozip"].Value = sam.ShiptoZip;
            cmd.Parameters.Add(new SqlParameter("@samplenumber", System.Data.SqlDbType.VarChar));
            cmd.Parameters["@samplenumber"].Value = sam.SampleNumber;
            //Console.WriteLine(cmd.CommandText);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public static string GetCustomerRepresentative(string email)
        {
            string uname = email.Split('@')[0];
            //Console.WriteLine(uname);
            DirectoryEntry connectionStart = new DirectoryEntry("LDAP://10.0.0.6");
            DirectorySearcher ds = new DirectorySearcher(connectionStart);
            ds.Filter = "(samaccountname=" + uname + ")";
            SearchResult sr = ds.FindOne();
            if (sr != null)
            {
                DirectoryEntry de = sr.GetDirectoryEntry();
                if (de.Properties.Contains("assistant"))
                {
                    ds = new DirectorySearcher(connectionStart);
                    ds.Filter = "(" + de.Properties["assistant"].Value.ToString().Split(',')[0] + ")";
                    sr = ds.FindOne();
                    if (sr != null)
                    {
                        de = sr.GetDirectoryEntry();
                        return (de.Properties["mail"].Value.ToString());
                    }
                }
            }
            return email;
        }
        public static void ProcessingExplosion(string delnum, string filename)
        {
            string message = "";
            SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
            string defaultRecipient = "mmoore@westchestergear.com";
            MailMessage email = new MailMessage();
            email.To.Add(defaultRecipient);
            email.Subject = "UPS Address Update Exploded";
            email.From = new MailAddress("AddressUpdater@westchestergear.com");

            message += "Address for Delivery/Sample "+ delnum +" Has Caused an Error<br />";
            message += " The filename for the error was:  " + filename;

            email.Body = message;
            email.IsBodyHtml = true;
            smtp.Send(email);
        }
    }
}
