namespace VerifyAddresses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Delivery
    {
        [Key]
        [StringLength(15)]
        public string DeliveryNumber { get; set; }

        [StringLength(25)]
        public string ShiptoNumber { get; set; }

        [StringLength(50)]
        public string ShiptoName { get; set; }

        [StringLength(50)]
        public string ShiptoAddress { get; set; }

        [StringLength(50)]
        public string shiptoCity { get; set; }

        [StringLength(50)]
        public string ShiptoState { get; set; }

        [StringLength(15)]
        public string ShiptoZip { get; set; }

        [StringLength(50)]
        public string CustomerPO { get; set; }

        [StringLength(30)]
        public string StoreNumber { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(15)]
        public string ShippingType { get; set; }

        public DateTime DateDownloaded { get; set; }

        [StringLength(25)]
        public string LocationID { get; set; }

        [StringLength(20)]
        public string Route { get; set; }

        public decimal? ActualWeight { get; set; }

        [StringLength(40)]
        public string ProNumber { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Cartons { get; set; }

        [StringLength(100)]
        public string Inco1 { get; set; }

        [StringLength(50)]
        public string ShiptoName2 { get; set; }

        [StringLength(20)]
        public string PhoneNumber { get; set; }

        [StringLength(15)]
        public string SoldtoNumber { get; set; }

        [StringLength(50)]
        public string ShiptoCountry { get; set; }

        [StringLength(50)]
        public string Inco2 { get; set; }

        [StringLength(20)]
        public string BillAcct { get; set; }
        [StringLength(15)]
        public string BeforeUpdateZip { get; set; }
        [NotMapped]
        public string SalesRep { get; set; }
        [NotMapped]
        public bool Updated { get; set; }
    }
}
