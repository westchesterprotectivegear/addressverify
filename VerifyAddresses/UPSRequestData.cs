﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VerifyAddresses
{
    public class UPSRequestData
    {
        public UPSSecurityGroup UPSSecurity;
        public Transaction XAVRequest;
    }

    public class UPSSecurityGroup
    {
        public UserToken UsernameToken;
        public ServiceToken ServiceAccessToken;
    }
    public class UserToken
    {
        public string Username = "mmoore1984";
        public string Password = "w3stch3st3r!";
    }
    public class ServiceToken
    {
        public string AccessLicenseNumber = "1D519566448E5175";
    }
    public class Transaction
    {
        public RequestSettings Request;
        public string MaximumListSize = "10";
        public AddressKey AddressKeyFormat;
    }
    public class RequestSettings
    {
        public string RequestOption = "1";
        public TransReference TransactionReference;
    }
    public class TransReference
    {
        public string CustomerContext;
    }
    public class AddressKey
    {
        public string ConsigneeName;
        public string BuildingName;
        public string AddressLine;
        public string PoliticalDivision2;  //City
        public string PoliticalDivision1;  //State
        public string PostcodePrimaryLow;  //Zip
        public string CountryCode;
    }


}
