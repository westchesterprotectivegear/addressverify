namespace VerifyAddresses
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Verify : DbContext
    {
        public Verify()
            : base("name=Verify")
        {
        }

        public virtual DbSet<Delivery> Deliveries { get; set; }
        public virtual DbSet<Sample> Samples { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Delivery>()
                .Property(e => e.DeliveryNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ShiptoNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ShiptoName)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ShiptoAddress)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.shiptoCity)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ShiptoState)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ShiptoZip)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.CustomerPO)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.StoreNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.Weight)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ShippingType)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.LocationID)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.Route)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ActualWeight)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ProNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.Cartons)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.Inco1)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ShiptoName2)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.SoldtoNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.ShiptoCountry)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.Inco2)
                .IsUnicode(false);

            modelBuilder.Entity<Delivery>()
                .Property(e => e.BillAcct)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.SampleNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ShiptoNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ShiptoName)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ShiptoAddress)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.shiptoCity)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ShiptoState)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ShiptoZip)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.CustomerPO)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.StoreNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.Weight)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ShippingType)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.LocationID)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.Route)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ActualWeight)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ProNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.Cartons)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Sample>()
                .Property(e => e.Inco1)
                .IsUnicode(false);

            modelBuilder.Entity<Sample>()
                .Property(e => e.ShiptoName2)
                .IsUnicode(false);
        }
    }
}
